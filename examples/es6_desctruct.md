```ts

person = {
    name:'alice', //age:21,
    address:{ street:'sezamkowa' }, 
    company:{name:'acme'}, 
    score:[100,200,300]
}

getInfo = (person) => {
//     const name = person.name
//     const addressstreet = person.address.street
//     const company = person.company.name
    const {
//         name: name,
        name,
        address:{ street: addressstreet }, 
        company:{name: company},
        age = 'not given',
        score:[,,lastScore]
    } = person; 

    return `${name} (age ${age}) lives on ${addressstreet}
    ${name} works in ${company} and scored ${lastScore} in last match`
}

getInfo(person)
'alice (age not given) lives on sezamkowa\n    alice works in acme and scored 300 in last match'

```

```ts
person = {
    name:'alice', //age:21,
    address:{ street:'sezamkowa' }, 
    company:{name:'acme'}, 
    score:[100,200,300]
}

getInfo = ({
    name,
    address:{ street: addressstreet }, 
    company:{ name: company },
    age = 'not given',
    score:[,,lastScore]
}) => `${name} (age ${age}) lives on ${addressstreet} ` +
      `${name} works in ${company} and scored ${lastScore} in last match`;

getInfo(person)
```