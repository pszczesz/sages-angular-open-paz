import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthService } from './core/services/auth.service';
import { UserService } from './core/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'MusicApp';

  alert(msg: string) {
    // this.alert(msg)
    // alert(msg)
    // window.alert(msg)
    console.log(msg);

    // this.title = 'zmiana~!'
    // ERROR Error: NG0100: ExpressionChangedAfterItHasBeenChecked
    // Error: Expression has changed after it was checked. 
    // Previous value: 'szkolenie angular open!'. 
    // Current value: 'zmiana~!!'.. 
    // Find more at https://angular.io/errors/NG0100
  }

  constructor(public user: UserService,
    public auth: AuthService,
  ) { }

  logout() {
    this.auth.logout()
  }

}
