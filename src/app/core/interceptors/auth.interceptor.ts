import { ErrorHandler, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private auth: AuthService,
    private errorHandler: ErrorHandler,) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const authReq = request.clone({
      setHeaders: {
        // Authorization: `Bearer ${this.auth.getToken()}`
      }
    })

    return next.handle(authReq).pipe(
      catchError((error) => {
        if (!(error instanceof HttpErrorResponse)) {
          this.errorHandler.handleError(error)
          return throwError(new Error('Unexpected error'))
        }
        if (!isSpotifyError(error)) {
          return throwError(new Error('Server error'))
        }
        if (error.error.error.status === 401) {
          setTimeout(() => this.auth.login(), 2000)
          return throwError(new Error('Token expired - logging you out!'))
        }
        return throwError(new Error(error.error.error.message))
      })
    )
  }
}

// Chain of responsibility:
// HttpClient.get(r)
// HttpClient.next = A
// A.next = B
// B.next = C
// C.next = HttpBackend

// Type Guard
function isSpotifyError(error: HttpErrorResponse): error is SpotifyError {
  return 'error' in error.error && typeof error.error.error.message === 'string'
}

interface SpotifyError extends HttpErrorResponse {
  error: {
    error: { message: string, status: number }
  }
}