import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';
import { Playlist } from 'src/app/playlists/model/Playlist';
import { Playlists } from '../actions/playlist.actions';
import { MusicApiService } from '../services/music-api.service';
import { PlaylistsService } from '../services/playlists.service';

interface IPlaylistsState {
    items: Playlist[],
    selected?: Playlist
}

@State<IPlaylistsState>({
    name: 'playlists',
    defaults: {
        items: []
    }
})
@Injectable()
export class PlaylistsState {

    // constructor(private service: PlaylistsService) { }
    constructor(private api: MusicApiService) { }

    @Selector() static playlists(state: IPlaylistsState) {
        return state.items
    }


    @Action(Playlists.FetchAll)
    fetchAll(ctx: StateContext<IPlaylistsState>, action: Playlists.FetchAll) {
        const state = ctx.getState();

        return this.api.getCurrentUserPlaylists().pipe(tap(playlists => {
            ctx.setState({ ...state, items: playlists });
        }))
    }
    
    @Action(Playlists.Edit)
    edit(ctx: StateContext<IPlaylistsState>, action: Playlists.Edit) {
        const state = ctx.getState();

        return this.api.updatePlaylist(action.payload)
            .pipe(switchMap(() => {
                ctx.setState({ ...state, selected: action.payload });
                return ctx.dispatch(new Playlists.FetchAll())
            }))

    }
}