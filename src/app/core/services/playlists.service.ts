import { Injectable } from '@angular/core';
import { BehaviorSubject, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { Playlist } from 'src/app/playlists/model/Playlist';
import { MusicApiService } from './music-api.service';
import { playlistsMocks } from './playlistsMocks';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {
  private playlists = new BehaviorSubject<Playlist[]>(playlistsMocks)
  playlistsChanges = this.playlists.asObservable()

  constructor(
    private user: UserService,
    private api: MusicApiService) { }

  getUserPlaylists() {
    this.api.getCurrentUserPlaylists().subscribe(data =>
      this.playlists.next(data)
    )
  }

  getPlaylistById(id: Playlist['id']) {
    return this.api.getPlaylistById(id)
  }

  updatePlaylist(draft: Playlist) {
    return this.api.updatePlaylist(draft).pipe(
      tap(() => this.getUserPlaylists())
    )
  }

  createPlaylist(draft: Playlist) {
    const user = this.user.getProfile()
    if (!user) { throw 'No User ID!' }

    return this.api.createPlaylist(user.id, draft).pipe(
      tap(() => this.getUserPlaylists())
    )
  }

  deletePlaylist(id: Playlist['id']) {
    return this.api.removePlaylistById(id).pipe(
      tap(() => this.getUserPlaylists())
    )
  }




}
