import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isLoggedInStatus = new BehaviorSubject(false)
  public isLoggedIn = this.isLoggedInStatus.asObservable()

  constructor(private oauth: OAuthService) {
    this.oauth.configure(environment.authConfig)
    this.oauth.events.subscribe(event => {
      console.log(event)
    })
  }

  getToken() {
    return this.oauth.getAccessToken()
  }

  async init() {
    await this.oauth.tryLoginImplicitFlow({
      // preventClearHashAfterLogin: true
    })

    if (!this.oauth.getAccessToken()) {
      this.login()
    } else {
      this.isLoggedInStatus.next(true)
    }
  }

  login() {
    this.oauth.initLoginFlow();
  }

  logout() {
    this.oauth.logOut()
    this.isLoggedInStatus.next(false)
  }
}
