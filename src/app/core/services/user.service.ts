import { HttpClient } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserProfile } from '../model/user-profile';
import { AuthService } from './auth.service';
import { API_URL } from './tokens';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user = new BehaviorSubject<UserProfile | null>(null)
  user = this._user.asObservable()

  constructor(
    @Inject(API_URL) private api_url: string,
    private http: HttpClient,
    private auth: AuthService
  ) {

    this.auth.isLoggedIn.subscribe(loggedIn => {
      if (loggedIn) {
        this.updateUser()
      } else {
        this._user.next(null)
      }
    })
  }

  getProfile(){
    return this._user.getValue()
  }

  updateUser() {
    this.http.get<UserProfile>(`${this.api_url}/me`)
      .subscribe(user => this._user.next(user))
  }

}



