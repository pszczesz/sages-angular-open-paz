
export const playlistsMocks = [{
  id: '123',
  name: 'Service Playlist 123',
  public: true,
  description: 'ala ma kota'
}, {
  id: '234',
  name: 'Service Playlist 234',
  public: false,
  description: 'ala ma psa'
}, {
  id: '345',
  name: 'Service Playlist 345',
  public: true,
  description: 'ala ma kota'
}];
