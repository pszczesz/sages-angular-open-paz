import { InjectionToken } from '@angular/core';

const tokens = {};
export const API_URL = new InjectionToken<string>('API_URL');
export const INITIAL_ALBUMS_SEARCH = new InjectionToken<string>('INITIAL_ALBUMS_SEARCH');
