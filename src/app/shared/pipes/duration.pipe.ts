import { formatDate } from '@angular/common';
import { Inject, LOCALE_ID, Pipe, PipeTransform } from '@angular/core';



@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  constructor(@Inject(LOCALE_ID) private localeId: string) { }

  transform(value: number): string {
    return formatDate(value, 'mm:ss', this.localeId, '+0000')
  }

}
