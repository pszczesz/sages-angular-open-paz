import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'yesno',
  // pure: false
})
export class YesnoPipe implements PipeTransform {

  transform(value: boolean, yes = 'yes', no = 'no'): string {
    console.log('calculate');
    
    return value ? yes : no
  }

}
