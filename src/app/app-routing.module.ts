import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full', // prefix
    redirectTo: '/music/search'
  },
  {
    path: 'music',
    loadChildren: () => {
      return import('./music/music.module')
        .then(m => m.MusicModule)
    }
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // add # to URL - to prevent reload
    // useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
