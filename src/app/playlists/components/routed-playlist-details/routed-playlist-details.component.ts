import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, pluck } from 'rxjs/operators';
import { Track } from 'src/app/core/model/album';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-routed-playlist-details',
  templateUrl: './routed-playlist-details.component.html',
  styleUrls: ['./routed-playlist-details.component.scss']
})
export class RoutedPlaylistDetailsComponent implements OnInit {

  playlist = this.route.data.pipe(pluck('playlist'))

  tracks = this.playlist.pipe(
    map<Playlist, Track[] | undefined>(playlist => playlist.tracks?.items.map(i => i.track))
  )

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

}
