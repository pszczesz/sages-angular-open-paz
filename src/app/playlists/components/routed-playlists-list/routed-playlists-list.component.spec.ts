import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutedPlaylistsListComponent } from './routed-playlists-list.component';

describe('RoutedPlaylistsListComponent', () => {
  let component: RoutedPlaylistsListComponent;
  let fixture: ComponentFixture<RoutedPlaylistsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutedPlaylistsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutedPlaylistsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
