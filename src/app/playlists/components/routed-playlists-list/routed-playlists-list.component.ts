import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { concat, merge, Observable } from 'rxjs';
import { pluck, tap } from 'rxjs/operators';
import { Playlists } from 'src/app/core/actions/playlist.actions';
import { PlaylistsService } from 'src/app/core/services/playlists.service';
import { PlaylistsState } from 'src/app/core/store/playlists.state';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-routed-playlists-list',
  templateUrl: './routed-playlists-list.component.html',
  styleUrls: ['./routed-playlists-list.component.scss']
})
export class RoutedPlaylistsListComponent implements OnInit {

  constructor(
    // private service: PlaylistsService,
    private route: ActivatedRoute,
    // private store: Store
  ) { }

  @Select(PlaylistsState.playlists) playlists!: Observable<Playlist[]>;

  // playlists = merge(
  //   this.route.data.pipe(pluck('playlists')),
  //   this.service.playlistsChanges
  // )
  // .pipe(tap(console.log))

  ngOnInit(): void {
  //   this.store.dispatch(new Playlists.FetchAll()).subscribe()
  }

  remove(id: string) { }

}
