import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutedPlaylistEditorComponent } from './routed-playlist-editor.component';

describe('RoutedPlaylistEditorComponent', () => {
  let component: RoutedPlaylistEditorComponent;
  let fixture: ComponentFixture<RoutedPlaylistEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutedPlaylistEditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutedPlaylistEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
