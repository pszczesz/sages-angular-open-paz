import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { first, mapTo, pluck, switchMap, withLatestFrom } from 'rxjs/operators';
import { Playlists } from 'src/app/core/actions/playlist.actions';
import { PlaylistsService } from 'src/app/core/services/playlists.service';
import { PlaylistsState } from 'src/app/core/store/playlists.state';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-routed-playlist-editor',
  templateUrl: './routed-playlist-editor.component.html',
  styleUrls: ['./routed-playlist-editor.component.scss']
})
export class RoutedPlaylistEditorComponent implements OnInit {

  playlist = this.route.data.pipe(pluck('playlist'))

  constructor(
    private service: PlaylistsService,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store
  ) { }

  @Select(PlaylistsState.playlists) playlists!: Observable<Playlist[]>;


  ngOnInit(): void {
  }

  submit(draft: Playlist) {
    this.playlist.pipe(
      first(),
      switchMap(playlist => {
        const saved = {
          ...draft, id: playlist.id
        }
        return this.store.dispatch(new Playlists.Edit(saved)).pipe(
          withLatestFrom(this.playlists),
          mapTo(saved)
        )
      })
    ).subscribe((saved) => {
      this.router.navigate(['/playlists', saved.id, 'details']);
    })
  }

}
