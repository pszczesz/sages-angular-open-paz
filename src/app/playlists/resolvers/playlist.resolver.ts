import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { MusicApiService } from 'src/app/core/services/music-api.service';
import { PlaylistsService } from 'src/app/core/services/playlists.service';
import { Playlist } from '../model/Playlist';

@Injectable({
  providedIn: 'root'
})
export class PlaylistResolver implements Resolve<Playlist> {


  constructor(private service: PlaylistsService, private api: MusicApiService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Playlist> {

    const id = route.paramMap.get('id')
    if(!id){
      throw Error('No ID')
    }
    return this.service.getPlaylistById(id)
  }
}
