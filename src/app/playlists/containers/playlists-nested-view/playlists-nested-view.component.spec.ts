import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistsNestedViewComponent } from './playlists-nested-view.component';

describe('PlaylistsNestedViewComponent', () => {
  let component: PlaylistsNestedViewComponent;
  let fixture: ComponentFixture<PlaylistsNestedViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaylistsNestedViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsNestedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
