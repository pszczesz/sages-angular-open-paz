import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumDetailsContainer } from './containers/album-details/album-details.container';
import { AlbumSearchContainer } from './containers/album-search/album-search.container';
import { MusicComponent } from './music.component';

// /music/** 
const routes: Routes = [{
  path: '',
  component: MusicComponent,
  children: [
    {
      path: '', pathMatch: 'full', redirectTo: 'search'
    },
    {
      path: 'search',
      component: AlbumSearchContainer
    },
    // {
    //   path: 'albums',
    //   component: AlbumDetailsContainer
    // }
    {
      path: 'albums/:albumId',
      component: AlbumDetailsContainer
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule { }
